#!/bin/sh

# USAGE:
#     collapse-link LINK1 LINK2 …
# This script replaces any symbolic link given as argument by its target;
# the target is pulled (moved) from its original place, not copied.

set -e

for link ; do
	# if the argument given is not a symlink, ignore it:
	[ -h "$link" ] || continue

	# compute the target:
	dir=$(dirname -- "$link")
	target=$(readlink -- "$link")
	case "$target" in
		/*) abstarget="$target";;
		*)  abstarget="$dir/$target";;
	esac

	# mv refuses to replace a symlink by a directory:
	[ -d "$abstarget" ] && rm -fv -- "$link"

	# replace the symbolic link by its target:
	mv -Tfv -- "$abstarget" "$link"

	# on failure, restore the symlink:
	if [ $? -ne 0 ] ; then
		[ -d "$abstarget" ] && ln -sfv -- "$target" "$link"
	fi
done
