#!/bin/bash

# This script monitors the battery. It shows warnings and adjust power-saving
# parameters when the battery becomes low, and suspend/hibernate the computer
# when the battery becomes critically low.
#
# Note: This script is compatible with TLP (Linux Advanced Power Management) and
# complements it, since TLP does not affect screen brightness, nor suspending.
#     https://linrunner.de/en/tlp/docs/tlp-faq.html#conflict

# Battery thresholds, in percents.
THRESHOLD_LOW=20
THRESHOLD_VERY_LOW=10
THRESHOLD_CRITICAL=2

# Brightness at various stages of depletion, in percents.
BRIGHTNESS_NORMAL=25
BRIGHTNESS_SAVING=5

# Frequency at which to check the status of the battery.
FREQUENCY=30s

# This script depends on the program “acpi”.
command -V acpi >/dev/null || return 1

# This function rings the bell for the given duration.
# It relies on an external program being available, whose usage is as follows:
#     bell $percent $pitch $duration
# I have one among my personal scripts.
mybell() {
  duration="$1"
  command bell 25 800 "$duration"
}

# state machine:
#   3 = sufficient battery or charging
#   2 = low battery
#   1 = very low battery
#   0 = critically low battery
#   0 = suspended
state=3
normal_brightness="$BRIGHTNESS_NORMAL"

while true ; do

  sleep "$FREQUENCY"

  # parse ACPI output
  #
  # NOTE: the `grep` command filters out fake batteries. Such a case has been
  # observed with the USB dongle of a wireless keyboard, wrongly detected as
  # a battery with 0% charge.
  output="$(acpi | grep -v -F ' 0%, rate information unavailable')"
  # cut on first occurrence of ": "
  _battery_id="${output%%: *}"  # substring before (unused)
  output="${output#*: }"        # substring after
  # cut on first occurrence of ", "
  status="${output%%, *}"       # substring before
  output="${output#*, }"        # substring after
  # cut on first occurrence of ", "
  percentage="${output%%, *}"   # substring before (or whole string if no occurrence)
  _est_time="${output#*, }"     # substring after (or whole string ...) (unused)
  # strip the percent sign if it is the last character
  percentage="${percentage%\%}"

  # we treat "Not charging" (which happens just after the battery is plugged)
  # the same as "Discharging":
  if [ "$status" = 'Not charging' ] ; then
    status='Discharging'
  fi

  # Battery becomes charging, or charged enough.
  if [ "$state" -lt 3 -a \( "$status" != 'Discharging' -o "$percentage" -gt "$THRESHOLD_LOW" \) ] ; then
    state=3
    xbacklight -set "$normal_brightness"

  # Battery becomes low.
  elif [ "$state" -gt 2 -a "$status" = 'Discharging' -a "$percentage" -le "$THRESHOLD_LOW" ] ; then
    state=2
    mybell 100
    notify-send -u critical -i battery-good-symbolic \
      "Batterie à $percentage %" 'Je suis fatigué…'
    normal_brightness="$(xbacklight)"
    xbacklight -set "$BRIGHTNESS_SAVING"

  # Battery becomes very low.
  elif [ "$state" -gt 1 -a "$status" = 'Discharging' -a "$percentage" -le "$THRESHOLD_VERY_LOW" ] ; then
    state=1
    mybell 250
    # Note: notify-send is buggy, it picks the incorrect icon “battery-low”
    # from /usr/share/notify-osd/icons/ instead of “battery-low-symbolic”
    # from /usr/share/icons/.
    notify-send -u critical -i battery-low-symbolic \
      "Batterie à $percentage %" 'Mes paupières sont si lourdes…'

  # Battery becomes critically low.
  elif [ "$state" -gt 0 -a "$status" = 'Discharging' -a "$percentage" -le "$THRESHOLD_CRITICAL" ] ; then
    state=0
    mybell 500
    notify-send -u critical -i battery-caution-symbolic \
      "Batterie à $percentage %" 'Je vais dormir.'
    logger 'Battery critically low, going to sleep'
    # FIXME: Make sure this bypasses all existing inhibiters.
    systemctl hibernate

  fi

done
